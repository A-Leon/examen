package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RectanguloActivity extends AppCompatActivity {
    private Metodo metodo;
    private TextView nombre;
    private EditText base;
    private EditText altura;
    private TextView area;
    private TextView peri;
    private Button btncal;
    private Button btnreg;
    private  Button btnlimp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo);

        nombre =(TextView)  findViewById(R.id.textname);
        base= (EditText) findViewById(R.id.lblbase);
        altura= (EditText) findViewById(R.id.lblaltura);
        area =(TextView) findViewById(R.id.txtarea);
        peri =(TextView) findViewById(R.id.txtperi);
        btncal =(Button) findViewById(R.id.btcal);
        btnreg =(Button) findViewById(R.id.btnreg);
        btnlimp =(Button) findViewById(R.id.btnlimpiar);


        Bundle datos = getIntent().getExtras();

        nombre.setText("Mi nombre es: "+ datos.getString("nombre"));

        metodo=(Metodo) datos.getSerializable("metodo");

        btncal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( base.getText().toString().matches("")||altura.getText().toString().matches("")){
                    Toast.makeText(RectanguloActivity.this, "Datos vacíos", Toast.LENGTH_SHORT).show();
                }else{
                    float bas= Integer.valueOf(base.getText().toString());
                    float alt= Integer.valueOf(altura.getText().toString());
                    metodo.setAltura(alt);
                    metodo.setBase(bas);

                    area.setText(""+metodo.calculararea());
                    peri.setText(""+metodo.calcularperi());


                }
            }
        });

        btnreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RectanguloActivity.this,MainActivity.class);
                startActivity(intent);
                finish();


            }
        });

        btnlimp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                base.setText("");
                altura.setText("");
                area.setText("");
                peri.setText("");
            }
        });




    }
}
