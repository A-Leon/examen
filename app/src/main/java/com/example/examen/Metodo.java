package com.example.examen;

import java.io.Serializable;

public class Metodo implements Serializable {

    private float base;
    private float altura;

    public Metodo(float base, float altura) {
        this.base = base;
        this.altura = altura;
    }

    public Metodo() {
    }

    public float getBase() {
        return base;
    }

    public void setBase(float base) {
        this.base = base;
    }

    public float getAltura() {
        return altura;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

    public float calculararea(){
        float area = this.base * this.altura;
        return area;
    }

    public float calcularperi(){
        float peri= this.base+this.altura+this.altura+this.base;
        return peri;
    }
}
