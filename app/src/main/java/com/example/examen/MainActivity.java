package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;



public class MainActivity extends AppCompatActivity {
    private TextView LblNombre;
    private EditText txtNom;
    private Button Entrar;
    private Button Salir;
    private Metodo metodo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LblNombre =(TextView) findViewById(R.id.textname);
        txtNom =(EditText) findViewById(R.id.lblname);
        Entrar= (Button) findViewById(R.id.btnentrar);
        Salir =(Button) findViewById(R.id.btnsalir);
        metodo =(Metodo) new Metodo();

        Entrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txtNom.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this,
                            "Faltó capturar Nombre",
                            Toast.LENGTH_SHORT).show();

                }else{
                    String name = txtNom.getText().toString();
                    Intent intent = new Intent(MainActivity.this,
                            RectanguloActivity.class);

                    intent.putExtra("nombre",name );

                    Bundle objeto = new Bundle();
                    objeto.putSerializable("metodo",metodo);
                    intent.putExtras(objeto);
                    startActivity(intent);
                    finish();




                }
            }
        });

        Salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
